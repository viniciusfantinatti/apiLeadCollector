package br.com.lead.collector.controllers;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.LeadService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(LeadController.class)
public class LeadControllerTest {

    @MockBean
    private LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    Lead lead;
    Produto produto;
    List<Produto> lstProduto;


    @BeforeEach
    public void inicializa(){
        lead = new Lead();
        lead.setNome("Vinicius");
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setEmail("vinicius@gmail.com");

        produto = new Produto();
        produto.setDescricao("Café do boum");
        produto.setId(1);
        produto.setNome("Café");
        produto.setPreco(30.00);

        lstProduto = new ArrayList<>();
        lstProduto.add(produto);

        lead.setListaProdutos(lstProduto);
    }

    @Test
    public void testarResgistrarLeadValido() throws Exception {
        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).then(leadObjeto -> {
            lead.setId(1);
            lead.setData(LocalDate.now());
            return lead;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", CoreMatchers.equalTo(LocalDate.now().toString())));
    }

        /* exemplo de conversão de json para DTO
            ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", CoreMatchers.equalTo(LocalDate.now().toString())));

        MvcResult mvcResult = resultActions.andReturn();
        String jsonResponsta = mvcResult.getResponse().getContentAsString();
        LeadDTO leadDTO = mapper.readValue(jsonResponsta, LeadDTO.class);

     */


}
