package br.com.lead.collector.services;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class LeadServiceTest {

    @MockBean
    private LeadRepository leadRepository;
    @MockBean
    private ProdutoService produtoService;

    @Autowired
    private LeadService leadService;

    Lead lead;
    Produto produto;
    List<Produto> lstProduto;


    @BeforeEach
    public void inicializar(){
        lead = new Lead();
        lead.setId(1);
        lead.setNome("Vinicius");
        lead.setData(LocalDate.now());
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setEmail("vinicius@gmail.com");

        produto = new Produto();
        produto.setDescricao("Café do boum");
        produto.setId(1);
        produto.setNome("Café");
        produto.setPreco(30.00);

        lstProduto = new ArrayList<>();
        lstProduto.add(produto);

        lead.setListaProdutos(lstProduto);
    }

    @Test
    public void testarBuscarPorTodosOsLeads() {
        Iterable<Lead> leads = Arrays.asList(lead);
        Mockito.when(leadRepository.findAll()).thenReturn(leads);

        Iterable<Lead> leadIterable = leadService.buscarTodos();

        Assertions.assertEquals(leads, leadIterable);
    }

    @Test
    public void testarSalvarLead() {
        Mockito.when(produtoService.consultatTodosPorId(Mockito.anyList())).thenReturn(lstProduto);

        Lead leadTeste = new Lead();
        leadTeste.setNome("Jeff");
        leadTeste.setListaProdutos(lstProduto);
        leadTeste.setEmail("jeff@mt.com.br");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);

        //Mockito com lambda
        Mockito.when(leadRepository.save(leadTeste)).then(leadLamb -> {
            // Simula o preenchimento do id como se fosse o banco de dados;
            leadTeste.setId(1);
            return leadTeste;
        });

        Lead leadObjeto = leadService.salvarLead(leadTeste);

        Assertions.assertEquals(LocalDate.now(), leadObjeto.getData());
        Assertions.assertEquals(1, leadTeste.getId());
        Assertions.assertEquals(produto, leadObjeto.getListaProdutos().get(0));
    }

    @Test
    public void testarDeletarLead() {
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(true);

        leadService.deletarLead(8001);

        // Verifica quatas vezes esse methodo do repository foi chamado quando eu executei o meu metodo que quero testar
        // o leadService.deletarLead()
        Mockito.verify(leadRepository, Mockito.times(1)).deleteById(Mockito.anyInt());
    }

    @Test
    public void testarDeletarLeadNegativo() {
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(false);

        Assertions.assertThrows(RuntimeException.class, () -> {leadService.deletarLead(8001);});
    }


}
