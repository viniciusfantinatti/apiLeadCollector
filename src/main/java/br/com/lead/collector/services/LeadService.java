package br.com.lead.collector.services;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private ProdutoService produtoService;

    public Lead salvarLead(Lead lead){
        List<Integer> idProdutos = new ArrayList<>();
        LocalDate data = LocalDate.now();

        for(Produto produto: lead.getListaProdutos()){
            int id = produto.getId();
            idProdutos.add(id);
        }
        List<Produto> produtos = produtoService.consultatTodosPorId(idProdutos);

        lead.setListaProdutos(produtos);
        lead.setData(data);
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public Iterable<Lead> buscarTodos(){
        Iterable<Lead> leads = leadRepository.findAll();
        return leads;
    }

    public Iterable<Lead> buscarTodosPorTipoLead(TipoLeadEnum tipoLead){
        Iterable<Lead> leads = leadRepository.findAllByTipoLead(tipoLead);
        return leads;
    }

    public Lead buscarPorId(int id){
        Optional<Lead> optionalLead = leadRepository.findById(id);
        if (optionalLead.isPresent()){
            return optionalLead.get();
        }
        throw new RuntimeException("O lead não foi encontrado");
    }

    public Lead atualizarLead(int id, Lead lead){
        if(leadRepository.existsById(id)){
            lead.setId(id);
            Lead leadObjeto = salvarLead(lead);

            return leadObjeto;
        }
        throw new RuntimeException("O lead não foi encontrado");
    }

    public void deletarLead(int id){
        if(leadRepository.existsById(id)){
            leadRepository.deleteById(id);
        }else{
            throw new RuntimeException("O lead não foi encontrado");
        }
    }
}
