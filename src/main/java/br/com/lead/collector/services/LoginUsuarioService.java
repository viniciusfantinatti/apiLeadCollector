package br.com.lead.collector.services;

import br.com.lead.collector.models.Usuario;
import br.com.lead.collector.repositories.UsuarioRepository;
import br.com.lead.collector.security.LoginUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class LoginUsuarioService implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        Usuario usuario = usuarioRepository.findByEmail(email);
        if(usuario == null){
            throw new UsernameNotFoundException(email);
        }

        LoginUsuario loginUsuario = new LoginUsuario(usuario.getId(), usuario.getEmail(), usuario.getSenha());
        return loginUsuario;
    }
}
