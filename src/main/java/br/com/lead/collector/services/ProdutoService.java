package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    //CONSULTA
    public Iterable<Produto> consultarTodos(){
        Iterable<Produto> produtos = produtoRepository.findAll();
        return produtos;
    }

    public Produto consultarPorId(int id){
        Optional<Produto> optionalProduto = produtoRepository.findById(id);
        if (optionalProduto.isPresent()){
            return optionalProduto.get();
        }
        throw new RuntimeException("O produto não foi encontrado!");
    }

    public List<Produto> consultatTodosPorId(List<Integer> lstProduto){
        Iterable<Produto> objProduto = produtoRepository.findAllById(lstProduto);
        return (List) objProduto;
    }

    public Iterable<Produto> consultarPorNome(String nome){
        Iterable<Produto> objetoProduto = produtoRepository.findAllByNome(nome);
        return objetoProduto;
    }

    public Iterable<Produto> consultarPorNomeContains(String nome){
        Iterable<Produto> objetoProduto = produtoRepository.findByNomeContains(nome);
        return objetoProduto;
    }


    //INCLUSÃO
    public Produto inserirProduto(Produto produto){
        Produto objetoProduto = produtoRepository.save(produto);
        return objetoProduto;
    }

    //ALTERAÇÃO
    public Produto alterarProduto(int id, Produto produto){
        if(produtoRepository.existsById(id)){
            produto.setId(id);
            Produto objetoProduto = inserirProduto(produto);
            return objetoProduto;
        }
        throw new RuntimeException("O produto não foi encontrado!");
    }

    //DELETE
    public void deletarProduto(int id){
        if(produtoRepository.existsById(id)){
            produtoRepository.deleteById(id);
        }else{
            throw new RuntimeException("O produto não foi encontrado!");
        }
    }
}
