package br.com.lead.collector.models;

import br.com.lead.collector.enums.TipoLeadEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Entity
@JsonIgnoreProperties(value = {"data"}, allowGetters = true)
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 5, max = 50, message = "Nome deve conter entre 5 e 50 caracteres")
    @NotNull(message = "Nome não pode ser nulo")
    @NotBlank(message = "Nome não pode ser branco")
    private String nome;

    @Email(message = "Email fora do padrão")
    @NotNull(message = "Email não pode ser nulo")
    private String email;
    private LocalDate data;
    @NotNull(message = "Tipo de Lead não pode ser nulo")
    private TipoLeadEnum tipoLead;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Produto> listaProdutos;

    public Lead() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public TipoLeadEnum getTipoLead() {
        return tipoLead;
    }

    public void setTipoLead(TipoLeadEnum tipoLead) {
        this.tipoLead = tipoLead;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Produto> getListaProdutos() {
        return listaProdutos;
    }

    public void setListaProdutos(List<Produto> listaProdutos) {
        this.listaProdutos = listaProdutos;
    }
}
