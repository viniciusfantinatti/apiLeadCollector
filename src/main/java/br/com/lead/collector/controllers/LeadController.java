package br.com.lead.collector.controllers;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/leads")
public class LeadController {

    @Autowired
    private LeadService leadService;

    @PostMapping
//    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Lead> registrarLead(@RequestBody @Valid Lead lead){
        Lead objLead = leadService.salvarLead(lead);
        return ResponseEntity.status(201).body(objLead);
    }

    @GetMapping
    public Iterable<Lead> exibirTodos(@RequestParam(name = "TipoDeLead", required = false) TipoLeadEnum tipoLead){
        if (tipoLead != null){
            Iterable<Lead> leads = leadService.buscarTodosPorTipoLead(tipoLead);
            return leads;
        }
        Iterable<Lead> leads = leadService.buscarTodos();
        return leads;
    }

    @GetMapping("/{id}")
    public Lead buscarPorId(@PathVariable int id){
        try{
            Lead lead =leadService.buscarPorId(id);
            return lead;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Lead atualizarLead(@PathVariable (name = "id") int id, @RequestBody Lead lead){
        try{
            Lead objetolead = leadService.atualizarLead(id, lead);
            return objetolead;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarLead(@PathVariable int id){
        try{
            leadService.deletarLead(id);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
