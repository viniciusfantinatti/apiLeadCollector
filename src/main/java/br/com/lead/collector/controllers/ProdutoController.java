package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/produto")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    //CONSULTA
    @GetMapping
    public Iterable<Produto> consultarTodos(@RequestParam(name = "nome", required = false) String nome) {
        if (nome == null) {
            Iterable<Produto> produtos = produtoService.consultarTodos();
            return produtos;
        } else if(nome.contains(nome)){
            Iterable<Produto> objetoProduto = produtoService.consultarPorNomeContains(nome);
            return objetoProduto;
        }else{
            Iterable<Produto> objetoProduto = produtoService.consultarPorNome(nome);
            return objetoProduto;
        }
    }


    @GetMapping("/{id}")
    public Produto buscarPorId(@PathVariable int id){
        try{
            Produto produto = produtoService.consultarPorId(id);
            return produto;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,exception.getMessage());
        }
    }

    //INCLUSÃO
    @PostMapping
    public Produto incluirProduto(@RequestBody Produto produto){
        return produtoService.inserirProduto(produto);
    }

    //ALTERAÇÃO
    @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable int id, @RequestBody Produto produto){
        try{
            Produto objetoProduto = produtoService.alterarProduto(id, produto);
            return objetoProduto;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,exception.getMessage());
        }
    }

    //DELEÇÃO
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletarProduto(@PathVariable int id){
        try{
            produtoService.deletarProduto(id);
            return ResponseEntity.status(204).body("{}");
        }catch(RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,exception.getMessage());
        }
    }

}
