package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Usuario;
import br.com.lead.collector.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {


    @Autowired
    private UsuarioService usuarioService;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario cadastrarUsuario(@RequestBody @Valid Usuario usuario){
        Usuario objUsuario = usuarioService.salvarUsuario(usuario);
        return objUsuario;

    }
}
